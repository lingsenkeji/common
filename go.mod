module gitee.com/lingsenkeji/common

go 1.16

require (
	github.com/pkg/errors v0.9.1
	github.com/zeromicro/go-zero v1.3.0
	google.golang.org/grpc v1.44.0
)
